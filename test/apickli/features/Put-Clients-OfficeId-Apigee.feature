@intg
Feature: Put Clients officeid ClientType
	As an API consumer
	I want to create charge requests
	So that I know they can be processed
	@Put-CreateAClientAndUpdate
    Scenario: Create a client and update
	    Given I set content-type header to application/json
	    And I create a client
	    When  I update the client with the body {"name": "Test123","privateKey": "Test123"}
	    Then response code should be 200
        And response body path $.name should be Test123
		
	@Put-CreateAClientAndUpdateReExecute
    Scenario: Create a client and update reexecute
	    Given I set content-type header to application/json
	    And I create a client
	    When  I update the client with the body {"name": "Test123","privateKey": "Test123"}
	    Then response code should be 200
        And response body path $.name should be Test123		
	
	@Put-CreateAClientAndUpdateWithoutName
    Scenario: Create a client and update without Name
	    Given I set content-type header to application/json
	    And I create a client
	    When  I update the client with the body {"officeIds": [1]}
	    Then response code should be 400
		And response body path $.message should be There was a problem with the request
        #And response body path $.name should be Test123	

	@Put-CreateAClientAndUpdateAll
    Scenario: Create a client and update All fields
	    Given I set content-type header to application/json
	    And I create a client
	    When  I update the client with the body {"id": 1, "name": "test1", "privateKey": "test1", "redirectUri": "www.google.com", "userType": "gatewayID", "permissions": ["Logs"], "scopes": ["gateway"]}
	    Then response code should be 200
        #And response body path $.name should be Test123	
