@intg
Feature: Post Clients ClientType
	As an API consumer
	I want to create charge requests
	So that I know they can be processed

	@Post-CreateClientsTypeCardPresentApi
    Scenario: Create a client by type CardPresentApi
        Given I set clientId header to `clientId`
        And I set content-type header to application/json
		And I set unique client body with officeIds
		When I use HMAC and POST to /clients/CardPresentApi
        Then response code should be 200
        And response header Content-Type should be application/json

	@Post-CreateClientsTypeMerchantOnBoarding
    Scenario: Create a client by type MerchantOnBoarding
        Given I set clientId header to `clientId`
        And I set content-type header to application/json
		And I set unique client body with officeIds
		When I use HMAC and POST to /clients/MerchantBoarding
        Then response code should be 200
        And response header Content-Type should be application/json
		
	@Post-CreateClientsTypeGateWay
    Scenario: Create a client by type GateWay
        Given I set clientId header to `clientId`
        And I set content-type header to application/json
		And I set unique client body with officeIds
		When I use HMAC and POST to /clients/GateWay
        Then response code should be 200
        And response header Content-Type should be application/json
		
	@Post-CreateClientsTypeMerchantBoardingAddingAcopeAndPermission
    Scenario: Create a client by type MerchantBoarding by adding scope and permission
        Given I set clientId header to `clientId`
        And I set content-type header to application/json
		When I set unique client body with data "name": "TestOneScope","privateKey": "TestScopeKey","permissions": ["logs"],"scopes": ["gateway"],"officeIds": [907861] 
		And I use HMAC and POST to /clients/MerchantBoarding
        Then response code should be 200
        And response header Content-Type should be application/json
		
	@Post-CreateClientsTypeMerchantBoardingWithoutPrivateKey
    Scenario: Create a client by type MerchantBoarding Without private key
        Given I set clientId header to `clientId`
        And I set content-type header to application/json
		When I set unique client body with data "name": "TestOneScope","officeIds": [907861] 
		And I use HMAC and POST to /clients/MerchantBoarding
        Then response code should be 200
        And response header Content-Type should be application/json

	@Post-CreateClientsTypeMerchantBoardingWithoutName
    Scenario: Create a client by type MerchantBoarding Without Name Tag
        Given I set clientId header to `clientId`
        And I set content-type header to application/json
		When I set unique client body with data "officeIds": [907861] 
		And I use HMAC and POST to /clients/MerchantBoarding
        And response header Content-Type should be application/json
		And response body path $.message should be Internal Server Error
		
	@Post-CreateClientsTypeMerchantBoardingWithoutOfficeID
    Scenario: Create a client by type MerchantBoarding Without Offiec ID
        Given I set clientId header to `clientId`
        And I set content-type header to application/json
		When I set unique client body with data "name": "TestOneScope"
		And I use HMAC and POST to /clients/MerchantBoarding
        Then response code should be 400
        And response header Content-Type should be application/json
		And response body path $.code should be 400000
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more.
		And response body should contain "error":"invalid_request","error_description":"One or more OfficeIds are required for a MerchantBoarding client"
		
		 
 # "detail": {"error":"invalid_request","error_description":"One or more OfficeIds are required for a MerchantBoarding client"}
		
		
	#-----------------------Put - Clients-OfficeId-Apigee-----------------------------------------------

	@Put-CreateAClientAndUpdate
    Scenario: Create a client and update
	    Given I set content-type header to application/json
	    And I create a client
	    When  I update the client with the body {"name": "Test123","privateKey": "Test123"}
	    Then response code should be 200
        And response body path $.name should be Test123
		
	@Put-CreateAClientAndUpdateReExecute
    Scenario: Create a client and update reexecute
	    Given I set content-type header to application/json
	    And I create a client
	    When  I update the client with the body {"name": "Test123","privateKey": "Test123"}
	    Then response code should be 200
        And response body path $.name should be Test123		
	
	@Put-CreateAClientAndUpdateWithoutName
    Scenario: Create a client and update without Name
	    Given I set content-type header to application/json
	    And I create a client
	    When  I update the client with the body {"officeIds": [1]}
	    Then response code should be 400
		And response body path $.message should be There was a problem with the request
        #And response body path $.name should be Test123	

	@Put-CreateAClientAndUpdateAll
    Scenario: Create a client and update All fields
	    Given I set content-type header to application/json
	    And I create a client
	    When  I update the client with the body {"id": 1, "name": "test1", "privateKey": "test1", "redirectUri": "www.google.com", "userType": "gatewayID", "permissions": ["Logs"], "scopes": ["gateway"]}
	    Then response code should be 200
        #And response body path $.name should be Test123	


	#-----------------------GET Clients-Apigee-----------------------------------------------
	
	@Get-CreateClientAndGet
	Scenario: Post a client and get it
	    Given I set content-type header to application/json
	    And I create a client
	    When  I get that same client
	    Then response code should be 200	
	
	

	
	
	
	
	
	
	
	
		