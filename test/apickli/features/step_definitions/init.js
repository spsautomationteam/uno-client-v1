/* jshint node:true */
'use strict';

var factory = require('./factory.js');
var config = require('../../config/config.json');

var invalidClientId = config.bankcard.invalidClientId;
var clientId = config.bankcard.clientId;
var clientSecret = config.bankcard.clientSecret;
var basepath = config.bankcard.basepath;
var apiproxy = config.bankcard.apiproxy;

module.exports = function() {
    // cleanup before every scenario
    this.Before(function(scenario, callback) {
        this.apickli = factory.getNewApickliInstance();
        this.apickli.storeValueInScenarioScope("invalidClientId", invalidClientId);
        this.apickli.storeValueInScenarioScope("clientId", clientId);
        this.apickli.storeValueInScenarioScope("clientSecret", clientSecret);
		this.apickli.storeValueInScenarioScope("basepath", basepath);
        this.apickli.storeValueInScenarioScope("apiproxy", apiproxy);
        callback();
    });
};

