@intg
Feature: Get Clients ClientType
	As an API consumer
	I want to create charge requests
	So that I know they can be processed
	
	@Get-CreateClientAndGet
	Scenario: Post a client and get it
	    Given I set content-type header to application/json
	    And I create a client
	    When  I get that same client
	    Then response code should be 200	
		
	@Get-CreateClientAndGet
	Scenario: Get the client that not exists
	    Given I set content-type header to application/json
	    And I create a client
	    When  I get that same client
	    Then response code should be 200
		When  I delete that same client
	    Then response code should be 204	