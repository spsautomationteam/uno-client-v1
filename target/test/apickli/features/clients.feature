@intg
Feature: Create Clients
	As an API consumer
	I want to create charge requests
	So that I know they can be processed

	@post-clients
    Scenario: Post a uno client
        Given I set clientId header to `clientId`
        And I set content-type header to application/json
		And I set unique client body
		When I use HMAC and POST to /clients
        Then response code should be 200
        And response header Content-Type should be application/json

	@POST-client-by-clientType
	Scenario: Post a client by clientType MerchantBoarding
	    Given I set clientId header to `clientId`
        And I set content-type header to application/json
		And I set unique client body with officeIds
		When I use HMAC and POST to /clients/MerchantBoarding
        Then response code should be 200
        And response header Content-Type should be application/json
		
	@get-client
	Scenario: Post a client and get it
	    Given I set content-type header to application/json
	    And I create a client
	    When  I get that same client
	    Then response code should be 200

	@delete-client
	Scenario: Post a client and then delete it
	    Given I set content-type header to application/json
	    And I create a client
	    When  I get that same client
	    Then response code should be 200
		When  I delete that same client
	    Then response code should be 204

	@put-client
	Scenario: Post a client and Update (put) it
	    Given I set content-type header to application/json
	    And I create a client
	    When  I update the client with the body {"name": "Test123","privateKey": "Test123"}
	    Then response code should be 200
        And response body path $.name should be Test123
		