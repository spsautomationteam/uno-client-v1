/* jslint node: true */
'use strict';

var factory = require('./factory.js');
var hmacTools = new (require("../../../hmacTools.js"))();
var uniqueClientId = Date.now() / 1000 + '';

var createClient = function (apickli, callback) {
    var pathSuffix = "/clients";
    var url = apickli.domain + pathSuffix;

    uniqueClientId = Date.now() + '1';

    var body = '{ "clientId": "' + uniqueClientId + '", "name": "' + uniqueClientId + '", "privateKey": "' + uniqueClientId + '"}';
   // var body = JSON.stringify(body);

    apickli.setRequestBody(body);
    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
    var hmac = hmacTools.hmac(clientSecret, "POST", url, body, "", nonce, timestamp);
    apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
    apickli.addRequestHeader('nonce', nonce);
    apickli.addRequestHeader('timestamp', timestamp);
    apickli.addRequestHeader('Authorization', hmac);

    apickli.post(pathSuffix, function (err, response) {
        apickli.storeValueOfResponseBodyPathInScenarioScope('$.clientId', 'authorizationReference');
        console.log("REFERENCE: ", apickli.scenarioVariables.authorizationReference);
        apickli.setRequestBody("");
        callback();
    });
};


var createClientByType = function (apickli, clientType, callback) {
    var pathSuffix = "/clients";
    var url = apickli.domain + pathSuffix + "/" + clientType;

    uniqueClientId = Date.now() + '1';

    var body = '{ "clientId": "' + uniqueClientId + '", "name": "' + uniqueClientId + '", "privateKey": "' + uniqueClientId + '" , "officeIds": [907861]}';
    // var body = JSON.stringify(body);

    apickli.setRequestBody(body);
    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
    var hmac = hmacTools.hmac(clientSecret, "POST", url, body, "", nonce, timestamp);
    apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
    apickli.addRequestHeader('nonce', nonce);
    apickli.addRequestHeader('timestamp', timestamp);
    apickli.addRequestHeader('Authorization', hmac);

    apickli.post(pathSuffix, function (err, response) {
        apickli.storeValueOfResponseBodyPathInScenarioScope('$.clientId', 'authorizationReference');
        console.log("REFERENCE: ", apickli.scenarioVariables.authorizationReference);
        apickli.setRequestBody("");
        callback();
    });
};

var updateClient = function (apickli, authorizationReference, updateBody, callback) {
    var pathSuffix = "/clients/" + authorizationReference;
    var url = apickli.domain + pathSuffix;
    var body = updateBody;//JSON.stringify(updateBody);

    apickli.setRequestBody(body);
    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
    var hmac = hmacTools.hmac(clientSecret, "PUT", url, body, "", nonce, timestamp);
    apickli.headers['nonce'] = nonce;
    apickli.headers['timestamp'] = timestamp;
    apickli.headers['Authorization'] = hmac;

    apickli.put(pathSuffix, callback);
};

var getClient = function (apickli, authorizationReference, callback) {
    var pathSuffix = "/clients/" + authorizationReference;
    var url = apickli.domain + pathSuffix;

    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
    var hmac = hmacTools.hmac(clientSecret, "GET", url, "", "", nonce, timestamp);
    apickli.headers['nonce'] = nonce;
    apickli.headers['timestamp'] = timestamp;
    apickli.headers['Authorization'] = hmac;

    apickli.get(pathSuffix, callback);
};

var deleteClient = function (apickli, authorizationReference, callback) {
    var pathSuffix = "/clients/" + authorizationReference;
    var url = apickli.domain + pathSuffix;

    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
    var hmac = hmacTools.hmac(clientSecret, "DELETE", url, "", "", nonce, timestamp);
    apickli.headers['nonce'] = nonce;
    apickli.headers['timestamp'] = timestamp;
    apickli.headers['Authorization'] = hmac;

    apickli.delete(pathSuffix, callback);
};

var setUniqueBody = function (apickli, isOfficeIdsIncluded, callback)
{
    uniqueClientId = Date.now() + '2';
    var body = '{ "clientId": "' + uniqueClientId + '", "name": "' + uniqueClientId + '", "privateKey": "' + uniqueClientId + '"}';

    if (isOfficeIdsIncluded === true) {
        body = '{ "clientId": "' + uniqueClientId + '", "name": "' + uniqueClientId + '", "privateKey": "' + uniqueClientId + '" , "officeIds": [907861]}';
    }
	
    //var body = JSON.stringify(body);
    apickli.setRequestBody(body);
    callback();
}


var setUniqueBodyData = function (apickli, updateBody, callback)
{
    uniqueClientId = Date.now() + '4';
	var bodyData= updateBody;
    var body = '{ "clientId": "' + uniqueClientId + '", '+bodyData+' }';
    console.log("Body: ", body);
    apickli.setRequestBody(body);
    callback();
} 




module.exports = function () {

    this.Given(/^I create a client$/, function (callback) {
        createClient(this.apickli, callback);
    });

    this.Given(/^I create a client by clientType (.*)$/, function (clientType, callback) {
        createClientByType(this.apickli, clientType, callback);
    });

    this.When(/^I update the client with the body (.*)$/, function (updateBody, callback) {
        var authorizationReference = this.apickli.scenarioVariables.authorizationReference;
        // console.log("REFERENCE: ", authorizationReference );
        updateClient(this.apickli, authorizationReference, updateBody, callback);
    });

    this.When(/^I get that same client$/, function (callback) {
        var authorizationReference = this.apickli.scenarioVariables.authorizationReference;
         console.log("REFERENCE: ", authorizationReference );
        getClient(this.apickli, authorizationReference, callback);
    });

    this.When(/^I delete that same client$/, function (callback) {
        var authorizationReference = this.apickli.scenarioVariables.authorizationReference;
        console.log("REFERENCE: ", authorizationReference);
        deleteClient(this.apickli, authorizationReference, callback);
    });

    this.When(/^I set unique client body$/, function (callback) {

        setUniqueBody(this.apickli, false, callback);
    });

    this.When(/^I set unique client body with officeIds$/, function (callback) {

        setUniqueBody(this.apickli, true, callback);
    });

	this.When(/^I set unique client body with data (.*)$/, function (bodyData,callback) {

		setUniqueBodyData(this.apickli, bodyData, callback);
	}); 
	
};
