 //# --- GET ---

//Get auth values from KVM sps-configuration
var oauth_id = context.getVariable("sage.oauth_id");

var oauth_key = context.getVariable("sage.oauth_key");  

//# target. Get values from KVM sps-configuration
var target_host = context.getVariable("sage.target_host");
var target_path = context.getVariable("sage.target_path");
var target_pathsuffix = context.getVariable("sage.target.pathsuffix");

var url = 'https://' + target_host + target_path + target_pathsuffix;//+ '/tokens';

var verb = context.getVariable("request.verb");
var nonce = hmacTools.nonce(12); 

var req_body = context.getVariable('request.content');
if(!req_body || req_body == "{}")
{
    req_body = "";
}

//# --- create HMAC ---
var data = verb + url +  nonce + req_body;
var hmac = hmacTools.createHmac(oauth_key, data);

//# --- SET ---
context.setVariable("sage.target-request-body", req_body);
context.setVariable("sage.target-request-hmac", "HMAC " + hmac);
context.setVariable("sage.target-request-nonce", nonce);

