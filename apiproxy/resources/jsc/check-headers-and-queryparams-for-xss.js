// Set this for differentiation in FaultRules ScriptExecutionFailed fault
context.setVariable("sage.error.policy.name", "JS-Check-Headers-and-QueryParams-for-XSS");

// adaptation of the methodology shared here: https://community.apigee.com/comments/26861/view.html
 // uses sample blacklist patterns here: http://docs.apigee.com/api-services/reference/regular-expression-protection#usagenotes
[   
    "<\\s*script\\b[^>]*>[^<]+<\\s*/\\s*script\\s*>", // js injection
    "[\\s]*((delete)|(exec)|(drop\\s*table)|(insert)|(shutdown)|(update))", // sql injection
    "<!--\\s*<!--(include|exec|echo|config|printenv)\\s+.*", // something to do with servers
    ".*Exception in thread.*" // java exception injection. (apigee builds JS to Java, i believe.)
].forEach(function(val) {
    var rx = getRegexObject(val);
    checkCollection("header", rx);
    checkCollection("queryparam", rx);
});

function getRegexObject(str){
    return new RegExp(str, 'i');
}

function checkCollection(typeName, rx){
    var colPref = "request." + typeName;
    var colArr = context.getVariable(colPref + "s.names").toArray();
    colArr.forEach(function(key){
        var val = context.getVariable(colPref + "." + key);
        if (isMatch(rx, key) || isMatch(rx, val)){
            throwError(typeName, key);
        }
    });
}

function isMatch(regex, str){
    return regex.test(str);
}

function throwError(type, name){
    throw new Error(type + " " + name + " rejected as unsafe.");
}
